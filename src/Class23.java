final class Class23 {

    static int anInt452;
    static int anInt453 = 0;
    static int anInt454;
    static int anInt455;
    static int anInt456;
    static boolean[][] aBooleanArrayArray457;
    private static Class94 aClass94_458 = Class3_Sub4.method108("Opened title screen", (byte) -119);
    static Class94 aClass94_459 = aClass94_458;

    public static void method937(int var0) {
        try {
            aBooleanArrayArray457 = null;
            aClass94_458 = null;
            if (var0 != 0) {
                aBooleanArrayArray457 = ((boolean[][]) null);
            }

            aClass94_459 = null;
        } catch (RuntimeException var2) {
            throw Class44.method1067(var2, "dl.A(" + var0 + ')');
        }
    }

    static final void method938(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        try {
            if (var6 != 1) {
                aClass94_458 = null;
            }

            int var9 = var2 - var7;
            int var10 = var3 - var8;
            int var11 = (-var1 + var0 << 16) / var9;
            int var12 = (-var5 + var4 << 16) / var10;
            Class136.method1814(var1, var3, var2, var12, var7, 0, 0, 127, var11, var8, var5);
        } catch (RuntimeException var13) {
            throw Class44.method1067(var13, "dl.B(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ')');
        }
    }

    static final ByteBuffer method939(byte var0) {
        try {
            ByteBuffer var1 = new ByteBuffer(34);
            var1.putByte(11);
            var1.putByte(Class3_Sub28_Sub10.anInt3625);
            var1.putByte(!Class3_Sub28_Sub13.aBoolean3665 ? 0 : 1);
            var1.putByte(Class3_Sub28_Sub7.aBoolean3604 ? 1 : 0);
            var1.putByte(Class148.aBoolean1905 ? 1 : 0);
            var1.putByte(Class25.aBoolean488 ? 1 : 0);
            var1.putByte(!Class11.aBoolean236 ? 0 : 1);
            var1.putByte(!Class44_Sub1.aBoolean2623 ? 0 : 1);
            var1.putByte(Class3_Sub13_Sub22.aBoolean3275 ? 1 : 0);
            var1.putByte(!Class140_Sub6.aBoolean2910 ? 0 : 1);
            var1.putByte(Class80.anInt1137);
            var1.putByte(!Class106.aBoolean1441 ? 0 : 1);
            var1.putByte(Class128.aBoolean1685 ? 1 : 0);
            var1.putByte(Class38.aBoolean661 ? 1 : 0);
            var1.putByte(Class3_Sub28_Sub9.anInt3622);
            var1.putByte(!Class3_Sub13_Sub15.aBoolean3184 ? 0 : 1);
            var1.putByte(Class3_Sub16.anInt2453);
            var1.putByte(Class9.anInt120);
            var1.putByte(Class14.anInt340);
            var1.putShort(Class3_Sub13.anInt2378);
            var1.putShort(Class3_Sub13_Sub5.anInt3071);
            var1.putByte(Class127_Sub1.method1757());
            var1.putInt(Class94.anInt2148);
            var1.putByte(Class3_Sub28.anInt2577);
            var1.putByte(Class94.aBoolean2146 ? 1 : 0);
            var1.putByte(!Class15.aBoolean346 ? 0 : 1);
            var1.putByte(Class3_Sub20.anInt2488);
            var1.putByte(Class73.aBoolean1080 ? 1 : 0);
            var1.putByte(Class163_Sub3.aBoolean3004 ? 1 : 0);
            return var1;
        } catch (RuntimeException var2) {
            throw Class44.method1067(var2, "dl.C(" + var0 + ')');
        }
    }

    static final void method940(int var0, int var1) {
        try {
            if (var0 >= 101) {
                Class149 var2 = Class3_Sub28_Sub7_Sub1.aClass149_4047;
                synchronized (var2) {
                    Class3_Sub28_Sub7_Sub1.anInt4045 = var1;
                }
            }
        } catch (RuntimeException var5) {
            throw Class44.method1067(var5, "dl.D(" + var0 + ',' + var1 + ')');
        }
    }

}
