import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.*;
import java.awt.Container;
import java.awt.Dimension;
import java.net.URL;
import java.net.*;
import java.util.Properties;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ClientLoader extends Applet {

    public static Properties props = new Properties();
    public JFrame frame;
    public static String world;

    public static void main(String[] paramArrayOfString) {
        new ClientLoader();
    }

    public ClientLoader() {
        try {
            world = "1";
            frame = new JFrame("530 Client");
            frame.setLayout(new BorderLayout());
            frame.setResizable(true);
            JPanel jp = new JPanel();
            jp.setLayout(new BorderLayout());
            jp.add(this);
            jp.setPreferredSize(new Dimension(765, 503));
            this.frame.getContentPane().add(jp, "Center");
            this.frame.pack();
            this.frame.setVisible(true);
            props.put("worldid", world);
            props.put("members", "1");
            props.put("modewhat", "0");
            props.put("modewhere", "0");
            props.put("safemode", "0");
            props.put("game", "0");
            props.put("js", "1");
            props.put("lang", "0");
            props.put("affid", "0");
            props.put("lowmem", "0");
            props.put("settings", "kKmok3kJqOeN6D3mDdihco3oPeYN2KFy6W5--vZUbNA");
            Class87 sn = new Class87(this, 32, "runescape", 28);
            client.providesignlink(sn);
            client localclient = new client();
            localclient.init();
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    @Override
    public String getParameter(String paramString) {
        return ((String) props.get(paramString));
    }

    @Override
    public URL getDocumentBase() {
        return getCodeBase();
    }

    @Override
    public URL getCodeBase() {
        try {
            return new URL("http://67.168.49.236");
        } catch (MalformedURLException localException) {
            localException.printStackTrace();
            return null;
        }
    }
}
